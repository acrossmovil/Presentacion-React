// Import React
import React from "react";

// Import Spectacle Core tags
import {
  Appear,
  BlockQuote,
  Cite,
  CodePane,
  Deck,
  Fill,
  Heading,
  Image,
  Layout,
  Link,
  ListItem,
  List,
  Markdown,
  Quote,
  Slide,
  Spectacle,
  Text
} from "spectacle";

// Import image preloader util
import preloader from "spectacle/lib/utils/preloader";

// Import theme
import createTheme from "spectacle/lib/themes/default";

// Import custom component
import Interactive from "../assets/interactive";

// Require CSS
require("normalize.css");
require("spectacle/lib/themes/default/index.css");


const images = {
  city: require("../assets/city.jpg"),
  fondo: require("../assets/fondoNubox2.jpg"),
  kat: require("../assets/kat.png"),
  logo: require("../assets/formidable-logo.svg"),
  markdown: require("../assets/markdown.png"),
  LogoReact: require("../assets/logo react2.png"),
  DocReact: require("../assets/docreact.jpg"),
  FondoInternet: require("../assets/internet3.jpg"),
  Netflix: require("../assets/netflix.png"),
  paypal: require("../assets/paypal.png"),
  airbnb: require("../assets/airbnb.png"),
  bbc: require("../assets/bbc.png"),
  codecademy: require("../assets/codecademy.png"),
  discovery: require("../assets/discovery.png"),
  ebay: require("../assets/ebay.png"),
  facebook: require("../assets/facebook.png"),
  instagram: require("../assets/instagram.png"),
  nfl: require("../assets/nfl.png"),
  yahoo: require("../assets/yahoo.png"),
  salesforce: require("../assets/salesforce.png"),
  reddit: require("../assets/reddit.png"),
  walmart: require("../assets/walmart.png"),
  nyt: require("../assets/nyt.png"),
  

};

preloader(images);

const theme = createTheme({
  primary: "#C67A32",
  secundary: "#FFFFFF"
  });

export default class Presentation extends React.Component {
  render() {
    return (
      <Spectacle theme={theme}>
        <Deck transition={["zoom", "slide"]} transitionDuration={500}>
          <Slide transition={["zoom"]} bgImage={images.fondo.replace("/", "")} bgDarken={0.15}>
            <Heading size={1} fit caps lineHeight={1} textColor="#C67A32">
              ¿Por que React?
            </Heading>
            <Heading size={1} fit caps  textColor="secundary">
               ReactJS Libreria de Presentacion.
            </Heading>
          </Slide>
           <Slide transition={["zoom", "fade"]} bgImage={images.fondo.replace("/", "")} bgDarken={0.15}  notes="<ul><li>talk about that</li><li>and that</li></ul>">
            <Heading size={1} caps fit textColor="#C67A32">
                  Historia
              </Heading>
                <CodePane
                  lang="jsx"
                  source={require("raw!../assets/deck.example")}
                  />
             </Slide>
           <Slide transition={["slide"]} bgImage={images.fondo.replace("/", "")} bgDarken={0.15} notes="You can even put notes on your slide. How awesome is that?" bgColor="secondary" textColor="secundary">
          <Heading size={1} fit caps lineHeight={2} textColor="#C67A32">
        Caracteristicas
          </Heading>
            <Layout>
              <Fill>
              <List>
                <Appear><ListItem>Especifico para la vista</ListItem></Appear>
                <Appear><ListItem>Flexible</ListItem></Appear>
                <Appear><ListItem>Rapido</ListItem></Appear>
                <Appear><ListItem>Ligero</ListItem></Appear>
              </List>
              </Fill>
              <Fill>
                  <Image src={images.LogoReact.replace("/", "")} margin="0px auto 40px" height="193px"/>
              </Fill>
            </Layout>
          </Slide>
         <Slide transition={["slide"]} bgImage={images.city.replace("/", "")} bgDarken={0.69}>
             <Appear fid="1">
              <Heading size={1} caps fit textColor="primary">
               Composición de componentes
              </Heading>
            </Appear>
            <Appear fid="2">
              <Heading size={1} caps fit textColor="tertiary">
               Desarrollo Declarativo Vs Imperativo
              </Heading>
            </Appear>
            <Appear fid="3">
              <Heading size={1} caps fit textColor="primary">
               Flujo de datos unidireccional
              </Heading>
            </Appear>
             <Appear fid="4">
              <Heading size={1} caps fit textColor="tertiary">
               Isomorfismo
              </Heading>
            </Appear>
             <Appear fid="4">
              <Heading size={1} caps fit textColor="primary">
               Elementos y JSX
              </Heading>
            </Appear>
            <Appear fid="4">
              <Heading size={1} caps fit textColor="tertiary">
               Componentes con y sin estado
              </Heading>
            </Appear>
            <Appear fid="4">
              <Heading size={1} caps fit textColor="primary">
               Ciclo de vida de los componentes
              </Heading>
            </Appear>
          </Slide>
         <Slide transition={["slide"]} bgColor="primary">
            <Heading size={1} caps fit textColor="tertiary">
             Presentacion Interactiva con ReactJS
            </Heading>
            <Interactive/>
          </Slide>
         <Slide transition={["slide"]} bgImage={images.fondo.replace("/", "")} bgDarken={0.15}>
            <Appear fid="1">
              <Image src={images.Netflix.replace("/", "")} margin="0px  40px" height="100px"/>
             </Appear>
            <Appear fid="2">
              <Image src={images.paypal.replace("/", "")} margin="0px  40px" height="100px"/>
            </Appear>
            <Appear fid="3">
               <Image src={images.airbnb.replace("/", "")} margin="0px  40px" height="100px"/>
            </Appear>
             <Appear fid="4">
                <Image src={images.bbc.replace("/", "")} margin="0px  40px" height="100px"/>
             </Appear>
             <Appear fid="5">
                <Image src={images.codecademy.replace("/", "")} margin="0px  40px" height="100px"/>
             </Appear>
             <Appear fid="6">
                <Image src={images.discovery.replace("/", "")} margin="0px  40px" height="100px"/>
             </Appear>
             <Appear fid="7">
                <Image src={images.ebay.replace("/", "")} margin="0px  40px" height="100px"/>
             </Appear>
              <Appear fid="8">
                <Image src={images.facebook.replace("/", "")} margin="0px  40px" height="100px"/>
             </Appear>
             <Appear fid="9">
                <Image src={images.instagram.replace("/", "")} margin="0px  40px" height="100px"/>
             </Appear>
             <Appear fid="10">
                <Image src={images.nfl.replace("/", "")} margin="0px  40px" height="100px"/>
             </Appear>
              <Appear fid="11">
                <Image src={images.yahoo.replace("/", "")} margin="0px  40px" height="100px"/>
             </Appear>
             <Appear fid="12">
                <Image src={images.salesforce.replace("/", "")} margin="0px  40px" height="100px"/>
             </Appear>
             <Appear fid="13">
                <Image src={images.reddit.replace("/", "")} margin="0px  40px" height="100px"/>
             </Appear>
             <Appear fid="14">
                <Image src={images.walmart.replace("/", "")} margin="0px  40px" height="100px"/>
             </Appear>
             <Appear fid="15">
                <Image src={images.nyt.replace("/", "")} margin="0px  40px" height="100px"/>
             </Appear>
          </Slide>
          <Slide transition={["slide", "spin"]} bgColor="primary">
            <Heading caps fit size={1} textColor="tertiary">
                Conclusión
            </Heading>
          </Slide>
          <Slide transition={["fade"]} bgColor="secondary" textColor="primary">
            <List>
              <Appear><ListItem>React es una librería completa y con un punto de maduración óptimo para implementarla en muchos tipos de proyectos distintos.</ListItem></Appear>
              <Appear><ListItem>Nos permite un desarrollo ágil, ordenado y con una arquitectura mantenible, focalizada en un gran performance.</ListItem></Appear>
              <Appear><ListItem>Aunque React no se encarga de todas las partes necesarias para hacer una aplicación web compleja, la serie de componentes y herramientas diversas que se basan en React nos permite encontrar una alternativa capaz de hacer cualquier cosa que podríamos hacer con un complejo framework</ListItem></Appear>
            </List>
          </Slide>
          <Slide transition={["spin", "slide"]} bgImage={images.fondo.replace("/", "")} bgDarken={0.15}>
            <Heading size={1} caps fit lineHeight={1.5} textColor="primary">
              Gerencia de productos Nubox.
            </Heading>
             <Heading size={1} caps fit lineHeight={1.5} textColor="secundary">
             Gracias.
            </Heading>
           </Slide>
        </Deck>
      </Spectacle>
    );
  }
}
